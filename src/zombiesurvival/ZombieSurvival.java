/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival;

import java.io.IOException;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import zombiesurvival.server.Server;
import zombiesurvival.states.IngameState;
import zombiesurvival.states.State;

/**
 *
 * @author znix
 */
public class ZombieSurvival extends BasicGame {

    private final Stack<State> states = new Stack<>();

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        if (args.length > 0 && args[0].equals("--host")) {
            Server.main(null);
            return;
        }
        try {
            // get everything in the natives path
            HackyInjectNatives.injectNatives();

            AppGameContainer appgc;
            appgc = new AppGameContainer(new ZombieSurvival("Zombie Survival Game"));
            appgc.setDisplayMode(640, 480, false);
            appgc.setAlwaysRender(true);
            appgc.setShowFPS(false);
            appgc.start();
        } catch (Exception ex) {
            Logger.getLogger(ZombieSurvival.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ZombieSurvival(String title) {
        super(title);
    }

    public void addState(State state, GameContainer container) throws SlickException {
        states.add(state);
        state.init(container);
    }

    public State getCurrentState() {
        return states.peek();
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        Assets.init();
        addState(new IngameState(), container);
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        getCurrentState().update(container, delta);
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        getCurrentState().render(container, g);
    }

    // delgates
    @Override
    public void keyPressed(int key, char c) {
        getCurrentState().keyPressed(key, c);
    }

    @Override
    public void keyReleased(int key, char c) {
        getCurrentState().keyReleased(key, c);
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        getCurrentState().mouseMoved(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        getCurrentState().mouseDragged(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
        getCurrentState().mouseClicked(button, x, y, clickCount);
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        getCurrentState().mousePressed(button, x, y);
    }

    @Override
    public void controllerButtonPressed(int controller, int button) {
        getCurrentState().controllerButtonPressed(controller, button);
    }

    @Override
    public void controllerButtonReleased(int controller, int button) {
        getCurrentState().controllerButtonReleased(controller, button);
    }

    @Override
    public void controllerDownPressed(int controller) {
        getCurrentState().controllerDownPressed(controller);
    }

    @Override
    public void controllerDownReleased(int controller) {
        getCurrentState().controllerDownReleased(controller);
    }

    @Override
    public void controllerLeftPressed(int controller) {
        getCurrentState().controllerLeftPressed(controller);
    }

    @Override
    public void controllerLeftReleased(int controller) {
        getCurrentState().controllerLeftReleased(controller);
    }

    @Override
    public void controllerRightPressed(int controller) {
        getCurrentState().controllerRightPressed(controller);
    }

    @Override
    public void controllerRightReleased(int controller) {
        getCurrentState().controllerRightReleased(controller);
    }

    @Override
    public void controllerUpPressed(int controller) {
        getCurrentState().controllerUpPressed(controller);
    }

    @Override
    public void controllerUpReleased(int controller) {
        getCurrentState().controllerUpReleased(controller);
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        getCurrentState().mouseReleased(button, x, y);
    }

    @Override
    public void mouseWheelMoved(int change) {
        getCurrentState().mouseWheelMoved(change);
    }

}
