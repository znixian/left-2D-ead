/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.states;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import zombiesurvival.CollisionType;

import static zombiesurvival.NetCodes.*;
import zombiesurvival.Weapon;
import zombiesurvival.entity.Enemy;
import zombiesurvival.entity.Entity;
import zombiesurvival.entity.EntityRegistry;
import zombiesurvival.entity.ai.Pathfinder;
import zombiesurvival.net.AmmoManager;
import zombiesurvival.net.Player;
import zombiesurvival.particle.Particle;
import zombiesurvival.particle.ParticleRegistry;
import zombiesurvival.world.Block;
import zombiesurvival.world.BlockRegistry;
import zombiesurvival.world.BlockWall;
import zombiesurvival.world.Vector2i;

/**
 *
 * @author znix
 */
public class IngameState extends State {

    public static final int BLOCK_SIZE = 16;
    public static final int PLAYER_BORDER = 4;

    private final Player localPlayer = new Player();

    /**
     * Mouse positons
     */
    private double mx, my;

    private final double scale = 2;

    private final boolean[] keys = new boolean[0xFF];

    private Socket soc;
    private DataInputStream in;
    private DataOutputStream out;

    private int timeSinceSync;

    private final ArrayList<Player> players = new ArrayList<>();
    private final ArrayList<Entity> entities = new ArrayList<>();
    private final ArrayList<Particle> particles = new ArrayList<>();

    private boolean shoot;

    private final Weapon[] weapons = {Weapon.SHOTGUN, Weapon.SMG, Weapon.AR, Weapon.SNIPER_RIFLE};
//    private final int[] ammos = new int[weapons.length];
    private final AmmoManager ammo = new AmmoManager();
    private int weaponSelected;

    private long msLeft;
    private int waveCap;
    private int unspawned;

    private final Map<Vector2i, Block> blocks = new HashMap<>();

    private boolean buildMode;

    @Override
    public void init(GameContainer container) throws SlickException {
        try {
            soc = new Socket("localhost", 8917);
            in = new DataInputStream(new BufferedInputStream(soc.getInputStream()));
            out = new DataOutputStream(new BufferedOutputStream(soc.getOutputStream()));

            setWeapon(0);

            out.write(WORLD_DOWNLOAD);
            out.write(NET_RFLUSH);
            out.flush();

            int blockCount = in.readInt();
            for (int i = 0; i < blockCount; i++) {
                Vector2i pos = new Vector2i(in);
                Block block = BlockRegistry.read(in);
                blocks.put(pos, block);
            }
        } catch (IOException ex) {
            Logger.getLogger(IngameState.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void displayReload() {
//        try {
//            out.write(ACT_RELOAD);
//        } catch (IOException ex) {
//            Logger.getLogger(IngameState.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    private boolean canMoveTo(double x, double y) {
        int bx = Pathfinder.posOnGridAbsolute(x);
        int by = Pathfinder.posOnGridAbsolute(y);
        Block block = blocks.get(new Vector2i(bx, by));
        return block == null || !block.isSolid();
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        try {
            particles.stream().forEach((shot) -> {
                shot.update(delta);
            });
            particles.removeIf((t) -> t.readyForRemoval());

            localPlayer.shoot.update(delta);
            if (localPlayer.reload != null) {
                int reloadPrevFrame = localPlayer.reload.getFrame();
                localPlayer.reload.update(delta);
                if (localPlayer.clip == 0 && localPlayer.shoot.isStopped()) {
                    Weapon wep = weapons[weaponSelected];
                    int extAmmo = ammo.get(wep);
                    if (localPlayer.reload.isStopped() && localPlayer.reload.getFrame() == 0 && extAmmo > 0) {
                        localPlayer.reload.start();
                        displayReload();
                    }
                    if (localPlayer.reload.isStopped() && localPlayer.reload.getFrame() == localPlayer.reload.getFrameCount() - 1) {
                        localPlayer.clip = Math.min(localPlayer.weapon.getClipSize(), extAmmo);
//                        ammo.add(wep, -localPlayer.clip);
                        localPlayer.reload.setCurrentFrame(0);
                    }
                }
                if (localPlayer.reload.getFrame() == 1 && reloadPrevFrame != localPlayer.reload.getFrame()) {
                    out.write(ACT_RELOAD);
                    out.write(localPlayer.weapon.getClipID());
                }
            }

            double speed = delta * 0.05;
            if (keys[Input.KEY_W]) {
                if (canMoveTo(localPlayer.x, localPlayer.y - speed - PLAYER_BORDER)) {
                    localPlayer.y -= speed;
                }
            }
            if (keys[Input.KEY_S]) {
                if (canMoveTo(localPlayer.x, localPlayer.y + speed + PLAYER_BORDER)) {
                    localPlayer.y += speed;
                }
            }
            if (keys[Input.KEY_A]) {
                if (canMoveTo(localPlayer.x - speed - PLAYER_BORDER, localPlayer.y)) {
                    localPlayer.x -= speed;
                }
            }
            if (keys[Input.KEY_D]) {
                if (canMoveTo(localPlayer.x + speed + PLAYER_BORDER, localPlayer.y)) {
                    localPlayer.x += speed;
                }
            }

            boolean pickup = false;
            for (Entity ent : entities) {
                ent.update(delta);

                if (pickup == false && ent.collides(localPlayer.x, localPlayer.y, CollisionType.PICKUP)) {
                    pickup = true;
                }
            }

            timeSinceSync += delta;
            if (timeSinceSync > 1000 / 60.0) {
                timeSinceSync = 0;
//                long start = System.currentTimeMillis();
                if (shoot) {
                    shoot();
                }
                out.write(POS_UPDATE);
                localPlayer.write(out);

                out.write(POS_GET_ALL);
                out.write(ENT_GET_ALL);
                out.write(POS_GET_PARTICLES);
                out.write(ENV_WAVE);
                out.write(WORLD_BLOCK_UPDATES);
                out.write(ENV_AMMO_GET);
                if (pickup) {
                    out.write(ENV_CHK_PICKUP);
                }
                out.write(NET_RFLUSH);
                out.flush();

                int playerCount = in.read();
                players.clear();
                for (int i = 0; i < playerCount; i++) {
                    players.add(new Player(in));
                }

                int entCount = in.read();
                entities.clear();
                for (int i = 0; i < entCount; i++) {
                    entities.add(EntityRegistry.read(in));
                }

                int particleCount = in.read();
                for (int i = 0; i < particleCount; i++) {
                    particles.add(ParticleRegistry.read(in));
                }

                if (in.read() == 1) {
                    waveCap = in.readInt();
                    unspawned = in.readInt();
                    msLeft = -1;
                } else {
                    msLeft = in.readInt();
                }

                int blockUpdateCount = in.readInt();
                for (int i = 0; i < blockUpdateCount; i++) {
                    Vector2i pos = new Vector2i(in);
                    Block block = BlockRegistry.read(in);
                    if (block == null) {
                        blocks.remove(pos);
                    } else {
                        blocks.put(pos, block);
                    }
                }

                int ammoCount = in.read();
                ammo.getAmmoMap().clear();
                for (int i = 0; i < ammoCount; i++) {
                    int id = in.read();
                    int amount = in.readInt();
                    ammo.set(Weapon.values()[id], amount);
                }
//                System.out.println("took " + (System.currentTimeMillis() - start) + "ms");
            }
        } catch (IOException ex) {
            Logger.getLogger(IngameState.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0); // exit. For now.
        }
    }

    private void shoot() throws IOException {
        if (!localPlayer.shoot.isStopped()) {
            return;
        }
        if (localPlayer.reload != null) {
            if (!localPlayer.reload.isStopped()) {
                return;
            }
            if (localPlayer.clip == 0) {
                return;
            }
        } else {
            Weapon wep = weapons[weaponSelected];
            if (ammo.get(wep) <= 0) {
                return;
            }
            ammo.add(wep, -1);
        }
        out.write(ACT_SHOOT);
        localPlayer.shoot.restart();
        if (localPlayer.reload != null) {
            localPlayer.clip--;
        }
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        g.setBackground(Color.lightGray);
        g.clear();

        g.setLineWidth(1);
        g.setColor(Color.white);

        if (buildMode) {

        } else {
            Weapon wep = weapons[weaponSelected];
            int ammoAmt = ammo.get(wep) - localPlayer.clip;
            if (localPlayer.reload != null) {
                g.drawString("Ammo: " + localPlayer.clip + " / " + ammoAmt, 10, container.getHeight() - 20);
            } else {
                g.drawString("Ammo: - / " + ammo.get(wep), 10, container.getHeight() - 20);
            }
            for (int i = 0; i < weapons.length; i++) {
                g.drawString((i == weaponSelected ? "*" : " ")
                        + "Slot " + i + ": "
                        + (weapons[i] != null ? weapons[i] : "none"), 10, container.getHeight() - 20 * 5 + 20 * i);
            }
        }

        float nis = (float) (scale);

        g.pushTransform();
        g.scale(nis, nis);

        for (Map.Entry<Vector2i, Block> block : blocks.entrySet()) {
            int x = block.getKey().x;
            int y = block.getKey().y;
            block.getValue().render(container, g, block.getKey(), x * BLOCK_SIZE, y * BLOCK_SIZE, buildMode);
        }
        // see http://stackoverflow.com/questions/14194345/finding-angle-from-center-point-on-circle
        localPlayer.rotation = Math.atan2(localPlayer.x * nis - mx, -localPlayer.y * nis + my) - Math.PI;

        if (buildMode) {
        } else {
            entities.stream().forEach((ent) -> {
                ent.render(container, g);
            });

            g.setColor(Color.black);
            particles.stream().forEach((particle) -> {
                particle.render(container, g);
            });
        }

        renderCharacter(container, g, localPlayer);

        players.stream().forEach((player) -> {
            renderCharacter(container, g, player);
        });

        g.popTransform();

        g.setColor(Color.black);

        g.fillRect(0, 0, container.getWidth(), 30);
        if (msLeft == -1) {
            int enemies = (int) entities.stream().filter((e) -> {
                return (e instanceof Enemy);
            }).count();
            
            int cap = Math.max(enemies + unspawned, waveCap);

            System.out.println("waveCap: " + cap + ", unspawned: " + unspawned + ", alive: " + enemies);

            int width = container.getWidth();
            int unspawnedWidth = width * unspawned / cap;
            int aliveWidth = width * enemies / cap;
            int deadWidth = width - unspawnedWidth - aliveWidth;

            g.setColor(Color.orange);
            g.fillRect(0, 0, unspawnedWidth, 30);
            g.setColor(Color.red);
            g.fillRect(unspawnedWidth, 0, aliveWidth, 30);
            g.setColor(Color.green);
            g.fillRect(unspawnedWidth + aliveWidth, 0, deadWidth, 30);
        } else {
            g.setColor(Color.white);
            String msg = "Next wave in " + (msLeft / 1000) + " seconds";
            int width = g.getFont().getWidth(msg);
            int height = g.getFont().getHeight(msg);

            g.drawString(msg, container.getWidth() / 2 - width / 2, 30 / 2 - height / 2);
        }
    }

    private void setWeapon(int index) {
        weaponSelected = index;
        Weapon wep = weapons[index];
        localPlayer.setWeapon(wep);

//        ammo.add(wep, 5);
        localPlayer.clip = 0;

        if (localPlayer.reload == null) {
            return;
        }

        localPlayer.shoot.stop();
        if (ammo.get(wep) == 0) {
            localPlayer.reload.stop();
        } else {
            localPlayer.reload.restart();
            displayReload();
        }
    }

    private void renderCharacter(GameContainer container, Graphics g, Player player) {
        float nis = (float) (16 * scale);
        float fx = (float) player.x, fy = (float) player.y;
        g.pushTransform();
        g.rotate(fx, fy, (float) (player.rotation * 180 / Math.PI));
        if (player.weapon == null) {
            return;
        }
//        g.drawImage(Assets.character, fx - 8, fy - 8);
        if (player.reload != null && !player.reload.isStopped()) {
            player.reload.draw(fx - 15.5f, fy - 26);
        } else {
            player.shoot.draw(fx - 15.5f, fy - 26);
        }
        g.popTransform();
        player.weapon.render(container, g, this, player.x, player.y, player.rotation, scale);
    }

    @Override
    public void keyPressed(int key, char c) {
//        if (key == Input.KEY_R && reload != null
//                && shoot.isStopped() && reload.isStopped() && ammos[weaponSelected] != 0) {
//            reload.restart();
//        }
        keys[key] = true;
    }

    @Override
    public void keyReleased(int key, char c) {
        keys[key] = false;
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        mx = newx;
        my = newy;
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        mx = newx;
        my = newy;
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        if (button == 2) {
            buildMode = !buildMode;
            return;
        }
        if (buildMode) {
            try {
                Block block;
                if (button == 0) {
                    block = new BlockWall();
                } else {
                    block = null;
                }

                out.write(WORLD_BLOCK_ADD);
                new Vector2i((int) (x / BLOCK_SIZE / scale), (int) (y / BLOCK_SIZE / scale)).write(out);
                out.writeInt(BlockRegistry.getBlockID(block));
                out.flush();

            } catch (IOException ex) {
                Logger.getLogger(IngameState.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            shoot = true;
        }
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        shoot = false;
    }

    @Override
    public void mouseWheelMoved(int change) {
        int newWeapon = weaponSelected;
        do {
            if (change < 0) {
                newWeapon++;
            } else {
                newWeapon--;
            }

            if (newWeapon < 0) {
                newWeapon = weapons.length - 1;
            }
            if (newWeapon >= weapons.length) {
                newWeapon = 0;
            }
        } while (weapons[newWeapon] == null);

        setWeapon(newWeapon);
    }

    public boolean isReloading() {
        return localPlayer.reload != null && !localPlayer.reload.isStopped();
    }

}
