/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.world;

import zombiesurvival.particle.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author znix
 */
public class BlockRegistry {

    private BlockRegistry() {
    }

    public static Block getBlockByID(int id) {
        if (id == 201) {
            return new BlockWall();
        } else {
            return null;
        }
    }

    public static int getBlockID(Block block) {
        if (block == null) {
            return 0;
        } else {
            return block.getID();
        }
    }

    public static void writeBlock(Block block, DataOutputStream out) throws IOException {
        if (block == null) {
            out.writeInt(0);
        } else {
            block.write(out);
        }
    }

    public static Block read(DataInputStream in) throws IOException {
        int id = in.readInt();
        Block e = getBlockByID(id);
        if (e != null) {
            e.read(in);
        }
        return e;
    }
}
