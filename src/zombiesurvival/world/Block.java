/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import zombiesurvival.Assets;
import static zombiesurvival.states.IngameState.BLOCK_SIZE;

/**
 *
 * @author znix
 */
public abstract class Block {

    public static final int MAX_PROGRESS = 100;

    protected int progress;

    public abstract boolean isSolid();

    public abstract int getID();

    public void write(DataOutputStream out) throws IOException {
        out.writeInt(getID());
        out.writeInt(progress);
    }

    void read(DataInputStream in) throws IOException {
        progress = in.readInt();
    }

    public void render(GameContainer container, Graphics g, Vector2i worldPos,
            int x, int y, boolean buildMode) throws SlickException {
        int height = BLOCK_SIZE * progress / MAX_PROGRESS;
        if (buildMode) {
            g.setColor(Color.yellow);
            g.drawRect(x, y, BLOCK_SIZE, BLOCK_SIZE);
            g.fillRect(x, y + BLOCK_SIZE - height, BLOCK_SIZE, height);
        } else {
//            g.drawImage(Assets.blocks[0], x, y);
            Image img = Assets.blocks[0];
            int imgHeight = img.getHeight();
            int unscaledHeight = imgHeight * progress / MAX_PROGRESS;
            g.drawImage(img,
                    x, y + BLOCK_SIZE - height, x + BLOCK_SIZE, y + BLOCK_SIZE,
                    0, img.getHeight() - unscaledHeight, img.getWidth(), imgHeight);
        }
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        if (progress > MAX_PROGRESS) {
            progress = MAX_PROGRESS;
        }
        if (progress < 0) {
            progress = 0;
        }
        this.progress = progress;
    }
}
