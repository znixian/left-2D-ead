/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.particle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 *
 * @author znix
 */
public abstract class Particle {

    public static final double DEFAULT_FADE_TIME = 1000;

    protected double x, y;
    protected double vx, vy;
    protected double fadeTime = DEFAULT_FADE_TIME;

    public Particle() {
    }

    public Particle(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Particle(double x, double y, double vx, double vy) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
    }

    /**
     * Client update. Only for visuals. Changes will NOT be sent back to the
     * server. Client side only
     *
     * @param delta ms since last update
     */
    public void update(int delta) {
        x += vx * delta;
        y += vy * delta;
        fadeTime -= delta;
    }

    public abstract void render(GameContainer container, Graphics g);

    public abstract int getID();

    public void read(DataInputStream in) throws IOException {
        x = in.readDouble();
        y = in.readDouble();
        vx = in.readDouble();
        vy = in.readDouble();
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(getID());
        out.writeDouble(x);
        out.writeDouble(y);
        out.writeDouble(vx);
        out.writeDouble(vy);
    }

    public boolean readyForRemoval() {
        return fadeTime < 0;
    }
}
