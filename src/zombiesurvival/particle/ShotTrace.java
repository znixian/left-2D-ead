/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.particle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 *
 * @author znix
 */
public class ShotTrace extends Particle {

    private double tx, ty;

    public ShotTrace(double x, double y, double tx, double ty) {
        this.x = x;
        this.y = y;
        this.tx = tx;
        this.ty = ty;
    }

    public ShotTrace() {
    }

    @Override
    public int getID() {
        return 110;
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        g.setColor(Color.white);
        g.drawLine((float) x, (float) y, (float) tx, (float) ty);
    }

    @Override
    public void read(DataInputStream in) throws IOException {
        super.read(in);
        tx = in.readDouble();
        ty = in.readDouble();
    }

    @Override
    public void write(DataOutputStream out) throws IOException {
        super.write(out);
        out.writeDouble(tx);
        out.writeDouble(ty);
    }

}
