/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.particle;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author znix
 */
public class ParticleRegistry {

    private ParticleRegistry() {
    }

    public static Particle getEntityByID(int id) {
        if (id == 110) {
            return new ShotTrace();
        } else if (id == 111) {
            return new SmallDroppedItem();
        } else {
            return null;
        }
    }

    public static Particle read(DataInputStream in) throws IOException {
        int id = in.read();
        Particle e = getEntityByID(id);
        e.read(in);
        return e;
    }
}
