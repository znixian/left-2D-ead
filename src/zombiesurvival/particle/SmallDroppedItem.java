/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.particle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import zombiesurvival.Assets;

/**
 *
 * @author znix
 */
public class SmallDroppedItem extends Particle {

    public int imgId;
    public double rotation;

    public SmallDroppedItem() {
        rotation = Math.random() * 360;
        fadeTime = 1000 * 30;
    }

    public SmallDroppedItem(int imgId, double x, double y, double vx, double vy) {
        super(x, y, vx, vy);
        this.imgId = imgId;
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        g.pushTransform();
        g.rotate((float) x + 4, (float) y + 4, (float) rotation);
        Image particles[] = Assets.smallParticles;
        if (imgId < 0 || imgId >= particles.length) {
            imgId = 1;
        }
        g.drawImage(Assets.smallParticles[imgId], (float) x, (float) y);
        g.popTransform();
    }

    @Override
    public int getID() {
        return 111;
    }

    @Override
    public void update(int delta) {
        double drag = 0.0005 * delta;
        if (vx > drag) {
            vx -= drag;
        } else if (vx < -drag) {
            vx += drag;
        } else {
            vx = 0;
        }
        if (vy > drag) {
            vy -= drag;
        } else if (vy < -drag) {
            vy += drag;
        } else {
            vy = 0;
        }
        super.update(delta);
    }

    @Override
    public void write(DataOutputStream out) throws IOException {
        super.write(out);
        out.writeInt(imgId);
    }

    @Override
    public void read(DataInputStream in) throws IOException {
        super.read(in);
        imgId = in.readInt();
    }

}
