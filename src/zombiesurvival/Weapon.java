/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival;

import java.io.IOException;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import zombiesurvival.net.Player;
import zombiesurvival.particle.SmallDroppedItem;
import zombiesurvival.server.Server;
import zombiesurvival.states.IngameState;

/**s
 *
 * @author znix
 */
public enum Weapon {

    SHOTGUN(0, Assets.shotgun, 300, null, 0, -1, 15, 5, 1) {
                @Override
                public void shoot(Server server, Player player) throws IOException {
                    emitShell(server, player);
                    for (int i = 0; i < 5; i++) {
                        server.shoot(player, (Math.random() - 0.5) * Math.PI / 8);
                    }
                }
            },
    SNIPER_RIFLE(5, Assets.sniper, 300, Assets.sniperRL, 500, 2, 2, 5, 0.25) {
                @Override
                public void shoot(Server server, Player player) throws IOException {
                    emitShell(server, player);
                    for (int i = 0; i < 10; i++) {
                        server.shoot(player, 0);
                    }
                }

                @Override
                public void render(GameContainer container, Graphics g, IngameState igs,
                        double x, double y, double rotation, double scale) {
                    double offset = igs.isReloading() ? 1.5 : 0.5;
                    double range = 500;
                    double start = 13;

                    x += Math.sin(rotation - Math.PI / 2) * offset;
                    y -= Math.cos(rotation - Math.PI / 2) * offset;

                    double sx = x + Math.sin(rotation) * start;
                    double sy = y - Math.cos(rotation) * start;

                    double ex = x + Math.sin(rotation) * range;
                    double ey = y - Math.cos(rotation) * range;

                    g.pushTransform();
                    g.setColor(Color.red);
                    g.setLineWidth((float) scale);
                    g.drawLine((float) sx, (float) sy, (float) ex, (float) ey);
                    g.popTransform();
                }
            },
    AR(15, Assets.ar, 50, Assets.arRL, 400, 1, 35, 45, 1),
    SMG(35, Assets.smg, 50, Assets.smgRL, 400, 3, 65, 85, 1) {
                @Override
                public void shoot(Server server, Player player) throws IOException {
                    emitShell(server, player);
                    server.shoot(player, (Math.random() - 0.5) * Math.PI / 16);
                }
            };

    private int clipSize;

    private Image[] images;
    private int timing;

    private Image[] reload;
    private int reloadTiming;

    /**
     * ID of the sprite used to render the clip when it is discarded.
     */
    private int clipID;

    private int basePickupAmount;
    private int randPickupAmount;

    /**
     * Weight to the pickup RNG. 1 is about normal, 0.5 is less likely, 2 is
     * more likely.
     */
    private double pickupChance;

    private Weapon() {
    }

    private Weapon(int clipSize, Image[] images, int timing, Image[] reload, int reloadTiming, int clipID, int basePickupAmount, int randPickupAmount, double pickupChance) {
        this.clipSize = clipSize;
        this.images = images;
        this.timing = timing;
        this.reload = reload;
        this.reloadTiming = reloadTiming;
        this.clipID = clipID;
        this.basePickupAmount = basePickupAmount;
        this.randPickupAmount = randPickupAmount;
        this.pickupChance = pickupChance;
    }

    public Animation getShootAnimation() {
        Animation anim = new Animation(images, timing);
        anim.setLooping(false);
        anim.setAutoUpdate(false);
        anim.setCurrentFrame(anim.getFrameCount() - 1);
        return anim;
    }

    public Animation getReloadAnimation() {
        if (reload == null) {
            return null;
        }
        Animation anim = new Animation(reload, reloadTiming);
        anim.setLooping(false);
        anim.setAutoUpdate(false);
        return anim;
    }

    /**
     * Get the number of rounds this gun can shoot before reloading. {@code -1}
     * for an infinite clip (ie, never reload) such as single-shot weapons.
     *
     * @return number of rounds or {@code -1}
     */
    public int getClipSize() {
        return clipSize;
    }

    public void shoot(Server server, Player player) throws IOException {
        server.shoot(player, 0);
        emitShell(server, player);
    }

    private static void emitShell(Server server, Player player) {
        double speed = 0.15;
        double distForward = 12;

        double rotation = player.rotation + (Math.random() - 0.5) * Math.PI / 2;

        double x = player.x + Math.sin(-rotation + Math.PI) * distForward;
        double y = player.y + Math.cos(-rotation + Math.PI) * distForward;

        double vx = Math.sin(-rotation + Math.PI / 2) * speed;
        double vy = Math.cos(-rotation + Math.PI / 2) * speed;

        server.addParticle(new SmallDroppedItem(0, x, y, vx, vy));
    }

    public void render(GameContainer container, Graphics g, IngameState igs,
            double x, double y, double rotation, double scale) {
    }

    public int getClipID() {
        return clipID;
    }

    public static Weapon getRandomWeapon() {
        Weapon weapons[] = values();
        double limits[] = new double[weapons.length];

        double max = 0;
        for (int i = 0; i < weapons.length; i++) {
            Weapon weapon = weapons[i];
            double prev = i == 0 ? 0 : limits[i - 1];
            double next = prev + weapon.pickupChance;
            limits[i] = next;
            max = next;
        }

        double rand = Math.random() * max;

        for (int i = 0; i < limits.length; i++) {
            double weap = limits[i];
            if (rand < weap) {
                return weapons[i];
            }
        }
        throw new RuntimeException("Weapon not found!");
    }

    public int getRandomAmmoDrop() {
        return (int) (basePickupAmount + Math.random() * randPickupAmount);
    }
}
