/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author znix
 */
public class Assets {

    public static Image character;
    public static Image shotgun[];
    public static Image ar[];
    public static Image arRL[];
    public static Image sniper[];
    public static Image sniperRL[];
    public static Image smg[];
    public static Image smgRL[];

    public static Image zombie;

    public static Image smallParticles[];

    public static Image blocks[];

    public static boolean isServer;

    public static void init() throws SlickException {
        Image characters_img = new Image("res/characters.png");
        characters_img.setFilter(Image.FILTER_NEAREST);
        SpriteSheet characters = new SpriteSheet(characters_img, 32, 32);

        character = characters.getSprite(0, 0);
        zombie = characters.getSprite(1, 0);

        shotgun = new Image[]{
            characters.getSprite(0, 1),
            characters.getSprite(0, 2),
            characters.getSprite(0, 0)
        };

        ar = new Image[]{
            characters.getSprite(2, 1),
            characters.getSprite(2, 2),
            characters.getSprite(2, 0)
        };

        arRL = new Image[]{
            characters.getSprite(3, 0),
            characters.getSprite(3, 1),
            characters.getSprite(3, 2),
            characters.getSprite(3, 3)
        };

        sniper = new Image[]{
            characters.getSprite(5, 2),
            characters.getSprite(5, 1),
            characters.getSprite(5, 0),
            characters.getSprite(5, 3)
        };

        sniperRL = new Image[]{
            characters.getSprite(4, 0),
            characters.getSprite(4, 1),
            characters.getSprite(4, 2),
            characters.getSprite(4, 3)
        };

        smg = new Image[]{
            characters.getSprite(6, 1),
            characters.getSprite(6, 2),
            characters.getSprite(6, 0)
        };

        smgRL = new Image[]{
//            characters.getSprite(7, 0),
//            characters.getSprite(7, 1),
            characters.getSprite(7, 2),
            characters.getSprite(7, 3),
            characters.getSprite(8, 0),
            characters.getSprite(8, 1),
            characters.getSprite(8, 2),
            characters.getSprite(7, 1)
        };

        /// particles sheet
        {
            Image particles_img = new Image("res/particles.png");
            particles_img.setFilter(Image.FILTER_NEAREST);
            SpriteSheet particles = new SpriteSheet(particles_img, 8, 8);

            int width = particles.getHorizontalCount();
            smallParticles = new Image[width * particles.getVerticalCount()];
            for (int i = 0; i < smallParticles.length; i++) {
                smallParticles[i] = particles.getSprite(i % width, i / width);
            }
        }

        /// block sheet
        {
            Image blocks_img = new Image("res/blocks.png");
            blocks_img.setFilter(Image.FILTER_NEAREST);
            SpriteSheet blocksSheet = new SpriteSheet(blocks_img, 16, 16);

            int width = blocksSheet.getHorizontalCount();
            blocks = new Image[width * blocksSheet.getVerticalCount()];
            for (int i = 0; i < blocks.length; i++) {
                blocks[i] = blocksSheet.getSprite(i % width, i / width);
            }
        }
    }

    private Assets() {
    }
}
