/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival;

/**
 *
 * @author znix
 */
public class NetCodes {

    public static final int NET_RFLUSH = 1;

    public static final int POS_UPDATE = 11;
    public static final int POS_GET_ALL = 12;
    public static final int POS_GET_PARTICLES = 13;

    public static final int ENT_GET_ALL = 21;

    public static final int ACT_SHOOT = 31;
    public static final int ACT_RELOAD = 32;
    
    public static final int ENV_WAVE = 41;
    public static final int ENV_AMMO_GET = 42;
    public static final int ENV_CHK_PICKUP = 43;
    
    public static final int WORLD_DOWNLOAD = 51;
    public static final int WORLD_BLOCK_ADD = 52;
    public static final int WORLD_BLOCK_UPDATES = 53;

    private NetCodes() {
    }
}
