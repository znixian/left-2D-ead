/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.net;

import java.util.HashMap;
import java.util.Map;
import zombiesurvival.Weapon;

/**
 *
 * @author znix
 */
public class AmmoManager {

    private final Map<Weapon, Integer> values;

    public AmmoManager() {
        values = new HashMap<>();
    }

    public int get(Weapon weapon) {
        return values.getOrDefault(weapon, 0);
    }

    public void set(Weapon weapon, int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negative ammo value of "
                    + amount + " for weapon " + weapon + "is not allowed!");
        }
        if (amount == 0) {
            values.remove(weapon);
        } else {
            values.put(weapon, amount);
        }
    }

    public void add(Weapon weapon, int amount) {
        set(weapon, get(weapon) + amount);
    }

    public Map<Weapon, Integer> getAmmoMap() {
        return values;
    }
}
