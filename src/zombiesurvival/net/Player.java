/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.newdawn.slick.Animation;
import zombiesurvival.Assets;
import zombiesurvival.Weapon;

/**
 *
 * @author znix
 */
public final class Player {

    public double x, y, rotation;

    public Weapon weapon;
    public Animation shoot;
    public Animation reload;
    public int clip;

    /**
     * Table of ammo amounts for this player. Stored only on the server.
     */
    public AmmoManager server_ammo = new AmmoManager();

    public Player() {
    }

    public Player(double x, double y, double rotation) {
        set(x, y, rotation);
    }

    public Player(DataInputStream in) throws IOException {
        read(in);
    }

    public void set(double x, double y, double rotation) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
    }

    public void read(DataInputStream in) throws IOException {
        set(in.readDouble(), in.readDouble(), in.readDouble());
        setWeapon(Weapon.values()[in.read()]);
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeDouble(x);
        out.writeDouble(y);
        out.writeDouble(rotation);

        if (weapon == null) {
            out.write(Weapon.SHOTGUN.ordinal());
        } else {
            out.write(weapon.ordinal());
        }
    }

    public void setWeapon(Weapon weapon) {
        if (weapon == this.weapon) {
            return;
        }

        this.weapon = weapon;

        if (Assets.isServer) {
            return;
        }

        shoot = weapon.getShootAnimation();
        reload = weapon.getReloadAnimation();

        if (reload != null) {
            reload.stop();
        }
    }
}
