/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Used to inject native libraries into the path at runtime.
 *
 * See
 * http://fahdshariff.blogspot.be/2011/08/changing-java-library-path-at-runtime.html
 */
public class HackyInjectNatives {

    private static boolean isInjected;
    private static final String OS = System.getProperty("os.name").toLowerCase();

    public static void injectNatives() throws Exception {
        if (isInjected) {
            return;
        }
        isInjected = true;

        String platform = null;

        if (isMac()) {
            platform = "macosx";
        } else if (isSolaris()) {
            platform = "solaris";
        } else if (isUnix()) {
            // saying that linux=unix, as the BSD folders were empty.
            platform = "linux";
        } else if (isWindows()) {
            platform = "windows";
        }

        System.out.println("Loading natives for platform: " + platform);

        addLibraryPath("lib/native/" + platform);
        addLibraryPath("distresources/native/" + platform);
        
        System.out.println("Done loading natives.");
    }

    /**
     * Adds the specified path to the java library path
     *
     * @param pathToAdd the path to add
     * @throws Exception
     */
    public static void addLibraryPath(String pathToAdd) throws Exception {
        final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
        usrPathsField.setAccessible(true);

        //get array of paths
        final String[] paths = (String[]) usrPathsField.get(null);

        //check if the path to add is already present
        for (String path : paths) {
            if (path.equals(pathToAdd)) {
                return;
            }
        }

        //add the new path
        final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
        newPaths[newPaths.length - 1] = pathToAdd;
        usrPathsField.set(null, newPaths);
    }

    public static boolean isWindows() {
        return OS.contains("win");
    }

    public static boolean isMac() {
        return OS.contains("mac");
    }

    public static boolean isUnix() {
        return OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
    }

    public static boolean isSolaris() {
        return OS.contains("sunos");
    }
}
