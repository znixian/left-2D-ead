/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.server;

import zombiesurvival.world.Vector2i;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import zombiesurvival.Assets;
import zombiesurvival.CollisionType;
import zombiesurvival.NetCodes;
import static zombiesurvival.NetCodes.*;
import zombiesurvival.Weapon;
import zombiesurvival.entity.Enemy;
import zombiesurvival.entity.Entity;
import zombiesurvival.entity.ItemDrop;
import zombiesurvival.entity.Zombie;
import zombiesurvival.entity.ai.Pathfinder;
import zombiesurvival.net.Player;
import zombiesurvival.particle.Particle;
import zombiesurvival.particle.ShotTrace;
import zombiesurvival.particle.SmallDroppedItem;
import static zombiesurvival.states.IngameState.BLOCK_SIZE;
import zombiesurvival.world.Block;
import zombiesurvival.world.BlockRegistry;

/**
 *
 * @author znix
 */
public class Server {

    private final ServerSocket server;
    private final List<Player> players = Collections.synchronizedList(new ArrayList<>());
    private final List<Entity> entities = Collections.synchronizedList(new ArrayList<Entity>());
    private final HashMap<Player, List<Particle>> particles = new HashMap<>();

    private final Map<Vector2i, Block> blocks = Collections.synchronizedMap(new HashMap<Vector2i, Block>());
    private final HashMap<Player, List<Vector2i>> blockUpdates = new HashMap<>();

    private long waveTime = -1;
    private int waveCap;
    private final int aliveCap = 5; // 20;
    private int unspawned;
    private long lastDroppedItem;

    private long msLastPathfind;

    public static void main(String[] args) throws IOException {
        Assets.isServer = true;
        Server server1 = new Server();
        server1.run();
    }

    public Server() throws IOException {
        server = new ServerSocket(8917);
        waveStart();
    }

    public void tick() {
        long time = System.currentTimeMillis();

        synchronized (entities) {
            entities.stream().forEach((entity) -> {
                entity.update(this);
            });
        }

        if (waveTime != -1 && waveTime < time) {
            waveTime = -1;
            waveStart();
        }

        if (waveTime == -1) {
            int spawn = Math.min(aliveCap - getAlive(), unspawned);
//            if (spawn > 0) {
//                spawn = 1;
//            }
            unspawned -= spawn;
            for (int i = 0; i < spawn; i++) {
                entities.add(new Zombie());
            }

            if (unspawned <= 0) {
                waveTime = time + 1000 * 1; // 30;
            }
        } else {
            if (lastDroppedItem + 1 * 1000 < time) { // 1 second
                lastDroppedItem = time;
                Vector2i pos = new Vector2i();
                int counter = 0;
                do {
                    pos.x = (int) (Math.random() * 20);
                    pos.y = (int) (Math.random() * 20);
                } while (blocks.get(pos) != null && counter++ < 100);
                if (counter < 100) {
                    entities.add(new ItemDrop(pos.x * BLOCK_SIZE, pos.y * BLOCK_SIZE));
                }
            }
        }

        if (msLastPathfind + 1000 < time) {
            msLastPathfind = time;

            Pathfinder.pathfind(this);
        }
    }

    private void waveStart() {
        unspawned = waveCap = 15; // 150
    }

    public void run() throws IOException {
        new Thread(() -> {
            try {
                long lastTime = System.currentTimeMillis();
                while (true) {
                    if (lastTime + 1000 / 60.0 > System.currentTimeMillis()) {
                        Thread.sleep(10);
                    } else {
                        tick();
                        lastTime += 1000 / 60.0;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();

//        for (int i = 0; i < 10; i++) {
//            entities.add(new Zombie());
//        }
        while (true) {
            Socket client = server.accept();
            new Thread(() -> {
                try {
                    DataInputStream in = new DataInputStream(new BufferedInputStream(client.getInputStream()));
                    DataOutputStream out = new DataOutputStream(new BufferedOutputStream(client.getOutputStream()));

                    Player player = new Player();
                    player.server_ammo.add(Weapon.AR, 10);

                    players.add(player);

                    while (true) {
                        int opcode = in.read();
                        if (opcode == NET_RFLUSH) {
                            out.flush();
                        } else if (opcode == POS_UPDATE) {
                            player.read(in);
                        } else if (opcode == POS_GET_ALL) {
                            out.write(players.size() - 1); // dont send them, so -1
                            for (Player player1 : players) {
                                if (player1 != player) {
                                    player1.write(out);
                                }
                            }
                        } else if (opcode == ENT_GET_ALL) {
                            synchronized (entities) {
                                out.write(entities.size());
                                for (Entity entity : entities) {
                                    entity.write(out);
                                }
                            }
                        } else if (opcode == ACT_SHOOT) {
                            if (player.weapon != null
                                    && player.server_ammo.get(player.weapon) > 0) {
                                player.weapon.shoot(this, player);
                                player.server_ammo.add(player.weapon, -1);
                            }
                        } else if (opcode == POS_GET_PARTICLES) {
                            if (!particles.containsKey(player)) {
                                out.write(0);
                            } else {
                                out.write(particles.get(player).size());
                                for (Particle part : particles.get(player)) {
                                    part.write(out);
                                }
                                particles.remove(player);
                            }
                        } else if (opcode == ACT_RELOAD) {
                            reloadParticle(player, in.read());
                        } else if (opcode == ENV_WAVE) {
                            if (waveTime == -1) {
                                out.write(1);
                                out.writeInt(waveCap);
                                out.writeInt(unspawned);
                            } else {
                                out.write(2);
                                out.writeInt((int) (waveTime - System.currentTimeMillis()));
                            }
                        } else if (opcode == NetCodes.WORLD_DOWNLOAD) {
                            out.writeInt(blocks.size());
                            for (Map.Entry<Vector2i, Block> block : blocks.entrySet()) {
                                block.getKey().write(out);
                                block.getValue().write(out);
                            }
                        } else if (opcode == WORLD_BLOCK_ADD) {
                            Vector2i pos = new Vector2i(in);
                            int id = in.readInt();
                            Block current = blocks.get(pos);
                            if (current != null && id != 0) {
                                current.setProgress(current.getProgress() + 10);
                            } else {
                                Block block = BlockRegistry.getBlockByID(id);
                                if (block == null) {
                                    blocks.remove(pos);
                                } else {
                                    blocks.put(pos, block);
                                }
                            }
                            blockUpdate(pos);
                        } else if (opcode == WORLD_BLOCK_UPDATES) {
                            if (!blockUpdates.containsKey(player)) {
                                out.writeInt(0);
                            } else {
                                out.writeInt(blockUpdates.get(player).size());
                                for (Vector2i pos : blockUpdates.get(player)) {
                                    Block block = blocks.get(pos);
                                    pos.write(out);
                                    BlockRegistry.writeBlock(block, out);
                                }
                                blockUpdates.remove(player);
                            }
                        } else if (opcode == ENV_AMMO_GET) {
                            if (player.server_ammo.getAmmoMap().isEmpty()) {
                                player.server_ammo.add(Weapon.SNIPER_RIFLE, 5);
                            }

                            out.write(player.server_ammo.getAmmoMap().size());
                            for (Map.Entry<Weapon, Integer> ammo
                                    : player.server_ammo.getAmmoMap().entrySet()) {
                                out.write(ammo.getKey().ordinal());
                                out.writeInt(ammo.getValue());
                            }
                        } else if (opcode == ENV_CHK_PICKUP) {
                            synchronized (entities) {
                                Entity ent = null;
                                for (Entity entity : entities) {
                                    if (entity.collides(player.x, player.y, CollisionType.PICKUP)) {
                                        ent = entity;
                                        break;
                                    }
                                }
                                if (ent != null) {
                                    entities.remove(ent);
                                    Weapon weapon = Weapon.getRandomWeapon();
                                    int amount = weapon.getRandomAmmoDrop();
                                    player.server_ammo.add(weapon, amount);
                                }
                            }
                        } else if (opcode == -1) {
                            break;
                        }
                    }

                    players.remove(player);

                    client.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        }
    }

    private int getAlive() {
        synchronized (entities) {
            int enemies = (int) entities.stream().filter((e) -> {
                return (e instanceof Enemy);
            }).count();
            return enemies;
        }
    }

    public void addParticle(Particle particle) {
        for (Player player : players) {
            List<Particle> part = Collections.singletonList(particle);
            particles.merge(player, part, (t, u) -> {
                if (t instanceof ArrayList) {
                    t.addAll(u);
                    return t;
                } else {
                    ArrayList<Particle> newshots = new ArrayList<>();
                    newshots.addAll(t);
                    newshots.addAll(u);
                    return newshots;
                }
            });
        }
    }

    public void blockUpdate(Vector2i pos) {
        for (Player player : players) {
            List<Vector2i> part = Collections.singletonList(pos);
            blockUpdates.merge(player, part, (t, u) -> {
                if (t instanceof ArrayList) {
                    t.addAll(u);
                    return t;
                } else {
                    ArrayList<Vector2i> newshots = new ArrayList<>();
                    newshots.addAll(t);
                    newshots.addAll(u);
                    return newshots;
                }
            });
        }
    }

    public void shoot(Player player, double rotOffset) {
        Entity raycast = raycast(player.x, player.y, player.rotation + rotOffset);
        if (raycast != null) {
            entities.remove(raycast);
//            entities.add(new Zombie());
        }
    }

    public Entity raycast(double xs, double ys, double rot) {
        // higher values will decrease server load, but
        // decrease precision.
        double speed = 2;

        double xd = Math.sin(rot) * speed;
        double yd = -Math.cos(rot) * speed;

        double xe = xs;
        double ye = ys;

        try {
            for (int i = 0; i < 500 / speed; i++) {
                synchronized (entities) {
                    for (Entity entity : entities) {
                        if (entity.collides(xe, ye, CollisionType.SHOOT)) {
                            return entity;
                        }
                    }
                }
                xe += xd;
                ye += yd;
            }
            return null;
        } finally {
            addParticle(new ShotTrace((int) xs, (int) ys, (int) xe, (int) ye));
        }
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public Map<Vector2i, Block> getBlocks() {
        return blocks;
    }

    private void reloadParticle(Player player, int type) {
        double speed = 0.15;
        double distForward = 12;
        double distSide = 6;

        double rotation = player.rotation;

        double x = player.x + Math.sin(-rotation + Math.PI) * distForward;
        double y = player.y + Math.cos(-rotation + Math.PI) * distForward;

        x += Math.sin(-rotation - Math.PI / 2) * distSide;
        y += Math.cos(-rotation - Math.PI / 2) * distSide;

        double vx = Math.sin(-rotation - Math.PI / 2) * speed;
        double vy = Math.cos(-rotation - Math.PI / 2) * speed;

        addParticle(new SmallDroppedItem(type, x, y, vx, vy));
    }
}
