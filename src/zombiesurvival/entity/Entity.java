/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.entity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.util.pathfinding.Path;
import zombiesurvival.CollisionType;
import zombiesurvival.server.Server;

/**
 *
 * @author znix
 */
public abstract class Entity {

    protected double x, y;
    protected double vx, vy;

    /**
     * Server side update.
     *
     * @param server
     */
    public void update(Server server) {
        int delta = 1000 / 60;
        x += vx * delta;
        y += vy * delta;
    }

    /**
     * Client side update. Only for visuals. Changes will NOT be sent back to
     * the server.
     *
     * @param delta ms since last update
     */
    public void update(int delta) {
        x += vx * delta;
        y += vy * delta;
    }

    public abstract void render(GameContainer container, Graphics g);

    public abstract int getID();

    public void read(DataInputStream in) throws IOException {
        x = in.readDouble();
        y = in.readDouble();
        vx = in.readDouble();
        vy = in.readDouble();
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(getID());
        out.writeDouble(x);
        out.writeDouble(y);
        out.writeDouble(vx);
        out.writeDouble(vy);
    }

    public boolean collides(double xs, double ys) {
        return xs > x && ys > y && xs < x + 16 && ys < y + 16;
    }

    public abstract boolean collides(double xs, double ys, CollisionType type);

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getVx() {
        return vx;
    }

    public double getVy() {
        return vy;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }
}
