/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.entity;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author znix
 */
public class EntityRegistry {

    private EntityRegistry() {
    }

    public static Entity getEntityByID(int id) {
        if (id == 10) {
            return new Zombie();
        } else if (id == 11) {
            return new ItemDrop();
        } else {
            return null;
        }
    }

    public static Entity read(DataInputStream in) throws IOException {
        int id = in.read();
        Entity e = getEntityByID(id);
        e.read(in);
        return e;
    }
}
