/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import zombiesurvival.CollisionType;

/**
 *
 * @author znix
 */
public class ItemDrop extends Entity {

    public ItemDrop() {
    }

    public ItemDrop(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean collides(double xs, double ys, CollisionType type) {
        if (type == CollisionType.PICKUP) {
            return collides(xs, ys);
        }
        return false;
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        g.drawRect((float) x, (float) y, 16, 16);
    }

    @Override
    public int getID() {
        return 11;
    }

}
