/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.Path.Step;
import zombiesurvival.CollisionType;
import zombiesurvival.entity.ai.Pathfinder;
import zombiesurvival.server.Server;
import zombiesurvival.states.IngameState;

/**
 *
 * @author znix
 */
public abstract class PathingEntity extends Entity {

    protected Path path;
    protected int step;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        boolean skip = false;
        if (path != null && path.getLength() >= 2 && this.path != null) {
            Path.Step goingToNow = this.path.getStep(step);
            Path.Step wouldBeNext = path.getStep(1);
            if (goingToNow.equals(wouldBeNext)) {
                skip = true;
            }
        }

        this.path = path;
        step = skip ? 1 : 0;
    }

    @Override
    public void update(Server server) {
        super.update(server);

        if (path == null) {
            vx = 0;
            vy = 0;
            return;
        }

        Path.Step next = path.getStep(step);
        double xdiff = IngameState.BLOCK_SIZE * next.getX() - x;
        double ydiff = IngameState.BLOCK_SIZE * next.getY() - y;

        if (Pathfinder.posOnGrid(x) == next.getX()
                && Pathfinder.posOnGrid(y) == next.getY()) {
            step++;
            if (step >= path.getLength()) {
                path = null;
            } else {
//                System.out.println("ok: " + step + "/" + path.getLength());
            }
            update(server);
            return;
        }

        double total = Math.abs(xdiff) + Math.abs(ydiff);

        total *= 50;

        if (total > 1) {
            xdiff /= total;
            ydiff /= total;
        }

        vx = xdiff;
        vy = ydiff;
    }

    @Override
    public void render(GameContainer container, Graphics g) {
//        if (path == null) {
//            return;
//        }
//        int x = (int) this.x;
//        int y = (int) this.y;
//        for (int i = step; i < path.getLength(); i++) {
//            Step nextStep = path.getStep(i);
//            int sx = nextStep.getX() * IngameState.BLOCK_SIZE;
//            int sy = nextStep.getY() * IngameState.BLOCK_SIZE;
//            g.drawLine(x, y, sx, sy);
//            x = sx;
//            y = sy;
//        }
    }

    @Override
    public boolean collides(double xs, double ys, CollisionType type) {
        if (type == CollisionType.SHOOT) {
            return collides(xs, ys);
        }
        return false;
    }
}
