/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.entity.ai;

import java.util.List;
import org.newdawn.slick.util.pathfinding.AStarHeuristic;
import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.Mover;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;
import zombiesurvival.entity.Entity;
import zombiesurvival.entity.PathingEntity;
import zombiesurvival.net.Player;
import zombiesurvival.server.Server;
import zombiesurvival.states.IngameState;
import zombiesurvival.world.Block;
import zombiesurvival.world.Vector2i;

/**
 *
 * @author znix
 */
public class Pathfinder {

    private Pathfinder() {
    }

    public static void pathfind(Server server) {
        TileBasedMap map = new TileBasedMap() {

            @Override
            public int getWidthInTiles() {
                return 200;
            }

            @Override
            public int getHeightInTiles() {
                return 200;
            }

            @Override
            public void pathFinderVisited(int x, int y) {
            }

            @Override
            public boolean blocked(PathFindingContext context, int tx, int ty) {
                Block b = server.getBlocks().get(new Vector2i(tx, ty));
                return b != null && b.isSolid();
            }

            @Override
            public float getCost(PathFindingContext context, int tx, int ty) {
                return 1;
            }
        };

        AStarPathFinder pathFinder = new AStarPathFinder(map, 50, false);

        List<Entity> entities = server.getEntities();
        synchronized (entities) {
        for (Entity entity : entities) {
            if (!(entity instanceof PathingEntity)) {
                continue;
            }
            PathingEntity e = (PathingEntity) entity;
//            if (e.getPath() != null) {
//                continue;
//            }
            boolean foundPath = false;
            for (Player player : server.getPlayers()) {
                Path findPath = pathFinder.findPath(null,
                        posOnGrid(e.getX()), posOnGrid(e.getY()),
                        posOnGrid(player.x), posOnGrid(player.y));
                if (findPath != null) {
                    foundPath = true;
                    e.setPath(findPath);
                    break;
                }
            }
            if(!foundPath) {
                e.setPath(null);
            }
        }
        }
    }

    public static int posOnGrid(double pos) {
        pos += IngameState.BLOCK_SIZE / 2;
        return posOnGridAbsolute(pos);
    }
    
    public static int posOnGridAbsolute(double pos) {
        return (int) (pos / IngameState.BLOCK_SIZE);
    }
}
