/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zombiesurvival.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import zombiesurvival.Assets;
import zombiesurvival.net.Player;
import zombiesurvival.server.Server;

/**
 *
 * @author znix
 */
public class Zombie extends PathingEntity implements Enemy {

    /**
     * Serverside update
     *
     * @param server
     */
    @Override
    public void update(Server server) {
        super.update(server);

//        for (Entity ent : server.getEntities()) {
//            double xdiff = ent.x - x;
//            double ydiff = ent.y - y;
//
//            double dist = xdiff * xdiff + ydiff * ydiff;
//
//            if (dist < 8 * 8) {
//                x -= xdiff;
//                y -= ydiff;
//            }
//        }
//        Player closest = null;
//        double closestDist = 0;
//        for (Player player : server.getPlayers()) {
//            double xdiff = player.x - x;
//            double ydiff = player.y - y;
//
//            double dist = xdiff * xdiff + ydiff * ydiff;
//
//            if (closest == null || dist < closestDist) {
//                closest = player;
//                closestDist = dist;
//            }
//        }
//        if (closest == null) {
//            vx = 0;
//            vy = 0;
//            return;
//        }
//
//        double xdiff = closest.x - x;
//        double ydiff = closest.y - y;
//        double total = Math.abs(xdiff) + Math.abs(ydiff);
//
//        if (total == 0) {
//            vx = 0;
//            vy = 0;
//            return;
//        }
//
//        total *= 50;
//
//        xdiff /= total;
//        ydiff /= total;
//
//        vx = xdiff;
//        vy = ydiff;
    }

    /**
     * Clientside render
     *
     * @param container
     * @param g
     */
    @Override
    public void render(GameContainer container, Graphics g) {
        super.render(container, g);
        g.drawImage(Assets.zombie, (float) x - 8, (float) y - 8);
    }

    @Override
    public int getID() {
        return 10;
    }

}
